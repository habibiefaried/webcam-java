import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Scanner;
import java.lang.Thread;
import javax.imageio.ImageIO;

import org.json.JSONObject;

class Koneksi
{
	private static SocketIO socket = null;
	
	public Koneksi(String address)
	{
		try
		{
			socket = new SocketIO(address);
	        socket.connect(new IOCallback() {
	            @Override
	            public void onMessage(JSONObject json, IOAcknowledge ack) {
	                //..algorithm
	            }
	
            
	            @Override
	            public void onMessage(String data, IOAcknowledge ack) {
	                System.out.println("Server said: " + data);
	            }
	
	            @Override
	            public void onError(SocketIOException socketIOException) {
	                System.out.println("an Error occured");
	                socketIOException.printStackTrace();
	            }
	
	            @Override
	            public void onDisconnect() {
	                System.out.println("Connection terminated.");
	            }
	
	            @Override
	            public void onConnect() {
	                System.out.println("Connection established");
	            }
	
	            @Override
	            public void on(String event, IOAcknowledge ack, Object... args) { //jalan terus
	            	try
	            	{
		            	System.out.println("ada event masuk : "+event);
		            }catch (Exception e){System.out.println("Error : "+e.getMessage());}
	            }
	        });
		
		}catch(Exception e){}
	}

	public static SocketIO getSocket(){return socket;}
}

class SendImage extends Thread
{
	private String gambar;
	private int periode;
	
	public SendImage(String _g, int _p)
	{
		gambar = _g;
		periode = _p;
	}
	
	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				// Entar taruh timer disini, biar bisa kirim gambar
				BufferedImage img = ImageIO.read(new File(gambar));
				String imgstr;
				imgstr = "data:image/jpeg;base64,";
				imgstr = imgstr + ImageUtils.encodeToString(img, "jpg");
			    JSONObject json = new JSONObject();
			    json.put("source",imgstr);
			    Koneksi.getSocket().emit("user image", json);
			    sleep(periode*1000); //periode dalam detik
			}
		}catch(Exception e){}
	}
}

public class Webcam
{
	public static void main(String args[])
	{
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.print("Masukkan address and port. Ex : http://<IP>:<PORT>/ : ");
		String address = sc.nextLine();
		System.out.print("Masukkan path gambar. Ex : /home/habibie/<gambar.jpg> : ");
		String gambar = sc.nextLine();
		System.out.print("Masukkan periode refresh image (dalam detik) : "); int periode = sc.nextInt();
		new Koneksi(address);
		new SendImage(gambar,periode).start();
	}
}